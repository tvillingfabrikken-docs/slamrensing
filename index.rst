.. master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Brukermanual for Slamrensings digitale tvilling
===================================================

Før vi kan begynne, må vi lage en bruker. Dette blir din personlige bruker som du vil både logge inn med, få tilgang til dine anlegg og registrere vedlikeholdsarbeid. 

**Steg 1: Registrere brukerprofil**

Gå til `app.tvillingfabrikken.no`_.
Nederst på innloggings siden står det "Sign up" med blå skrift. Registrer en profil med epost og passord. 

.. _`app.tvillingfabrikken.no`: https://app.tvillingfabrikken.no/

.. figure:: src/figures/auth.png
  :name: logo
  :width: 100%
  :align: center

**Steg 2: Be om tilgang**

Når du har registrert en brukerprofil, ta kontakt med Slamrensing for hvilke anlegg du skal ha tilgang til. Oppgi epostadressen på den registrerte brukerprofilen, og hvilke anlegg du skal ha tilgang til. 

**Steg 3: Oversikt over dine tilganger**

Når har du fått tilgang kan du se anleggene under «Home» i `app.tvillingfabrikken.no`_.
Klikk på anlegget du vil se. 
Anlegg som ikke er online vil være markert med rødt signalsymbol.

.. figure:: src/figures/home.png
  :name: logo
  :width: 100%
  :align: center


.. note::
  Vi anbefaler å laste ned mobilappen. På den måten er den alltid lett tilgjengelig når man er ute i felt og gjør vedlikehold eller feilsøking på anlegget.

  .. figure:: src/figures/downloadIOS.png
    :name: IOS
    :width: 20%
    :align: left
    :target: https://apps.apple.com/us/app/tvillingfabrikken/id6444527780

  .. figure:: src/figures/downloadAndroid.png
    :name: android
    :width: 20%
    :align: left
    :target: https://play.google.com/store/apps/details?id=com.Tvillingfabrikken.Tvillingfabrikken


**Steg 4: Navigering i den digitale tvillingen**

Naviger i den digitale tvillingen ved å holde inne venstre museknapp og dra rundt. 
Under "Assets" vises alle komponenter som pumper, regulatorer, beholdere, sensorer osv. 
Hvis det er feil på en komponent blir det markert med gult (advarsel) eller rødt (feil) rundt bildet. 
Du kan da trykke deg inn på komponenten og feilsøke. 
Trykk på "Home" for å komme tilbake til hovedsiden. 

.. figure:: src/figures/digitaltwin0.png
  :name: logo
  :width: 100%
  :align: center

Du kan også trykke på kompoenenter i 3D-modellen for å få opp sanntids måleverdier og detaljer for den aktuelle komponenten.

.. figure:: src/figures/digitaltwin1.png
  :name: logo
  :width: 100%
  :align: center


**Steg 5: Vedlikehold av anlegg.**

På menylisten under "Checklist" finner du vedlikeholdsrutiner som skal gjennomføres ukentlig. Her kan du se hva som skal utføres, hvordan de skal utføres og antall dager til fristen for utførelse. Når oppgaven er utført, legger operatøren til en kommentar om eventuelle avvik eller andre ting som kan være verdt å notere, og avslutter med å trykke "Submit". 
Utførte rutiner vil være synlig når du trykker på "Log".

.. figure:: src/figures/digitaltwin2.png
  :name: logo
  :width: 100%
  :align: center

**Steg 6: Historiske data**

En samling av sensordataene for de siste timene finnes under "Historian". Hvis du ønsker å gå dypere inn i dataen kan du gå til "Dashboard" i toppmenyen i web applikasjonen.
Her kan du se data fra anleggets oppstart.

.. figure:: src/figures/dashboard.png
  :name: logo
  :width: 100%
  :align: center

.. 
   .. toctree::
..    :numbered: 2
..    :maxdepth: 2
..    :caption: Contents

..    src/chapter1.rst
   .. src/chapter2.rst

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
